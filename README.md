# Principes FAIR pour les logiciels de recherche - FAIR4RS Principles

Traduction française des FAIR4RS Principles (Chue Hong, N. P., Katz, D. S., Barker, M., Lamprecht, A.-L., Martinez, C., Psomopoulos, F. E., Harrow, J., Castro, L. J., Gruenpeter, M., Martinez, P. A., Honeyman, T., et al. (2021). FAIR Principles for Research Software (FAIR4RS Principles). Research Data Alliance. DOI: 10.15497/RDA00065).

Contributeurs / Contributors :
- Dimitri Szabo
- Hervé Guillemin 
- Sandrine Sabatié
- Serguei Sokol
- Sophie Aubin
- Tovo Rabemanantsoa

ATTENTION : La traduction étant en cours, celle-ci n'est pour l'instant que disponible sur [cette branche](https://forgemia.inra.fr/cati-prosodie/fair4rs-fr/-/tree/2022-05-27-workshop)

WARNING : As the translation is ongoing it is currently only accessible on [this branch](https://forgemia.inra.fr/cati-prosodie/fair4rs-fr/-/tree/2022-05-27-workshop)
